# Bakalářská práce

OSM soubor: prague_6.osm(41 MB) - výřez stažen z https://overpass-api.de/api/map?bbox=14.3378,50.0933,14.4292,50.1189

## Spuštění

Před spuštěním programu je nutné nainstalovat všechny potřebné knihovny (vypsáno níže). Pro distribuce s APT lze využít skript `install.sh` pro nainstalování většiny knihoven, kromě knihovny pgRouting.

Dále je nutné stažení výřezu z OpenStreetMap (například pomocí `wget  https://overpass-api.de/api/map?bbox=14.3378,50.0933,14.4292,50.1189 -O file.osm`)

BASH skript `RUN.sh file.osm` nastaví PostgreSQL databázi (pomocí sudo), načte zadaný soubor do databáze a spustí nástroj pro obohacení grafu. Výsledný soubor je uložen ve složce `java` s názvem `export.geojson`.

## Knihovny
 * Java (JDK i JRE) >= 11
 * PostgreSQL >= 10
 * PostGIS >= 2.4
 * PostgreSQL podpora pro PostGIS objekty
 * osm2pgsql
 * proj-bin
 * Maven
 * pgRouting >= 3.0.0
    * Pro správné fungování doporučuji nejnovější verzi (např. kompilací z https://github.com/pgRouting/pgrouting )
    * Při tvorbě práce dostupná verze 3.0.0-rc1
