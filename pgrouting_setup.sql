CREATE TABLE planet_osm_ways AS 
SELECT osm_id, way 
    FROM planet_osm_line 
    WHERE highway IN ('tertiary','residential','cycleway','pedestrian',
            'footway','living_street','unclassified','tertiary_link','service',
            'track','steps','path','corridor')
        AND (foot IS NULL OR foot NOT IN ('no','private','customers'))
        AND (bicycle IS NULL OR bicycle NOT IN ('no','private','customers'))     
        AND (access IS NULL OR access NOT IN ('no','private','customers'))
        AND osm_id >= 0;
ALTER TABLE planet_osm_ways ADD COLUMN source bigint, ADD COLUMN target bigint;
SELECT pgr_nodeNetwork('planet_osm_ways', 1, 'osm_id', 'way');
SELECT pgr_createTopology('planet_osm_ways_noded', 1, 'way');
