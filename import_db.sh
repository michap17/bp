#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

if [[ $# -eq 0 ]]; then
    echo "Usage: ./import_db.sh file.osm"
    exit 1
fi

# Export PG* variables
export PGDATABASE='prague'
export PGUSER='petr'
export PGPASSWORD='post'

dropdb prague -i --if-exists
createdb prague
psql -c 'CREATE EXTENSION postgis; CREATE EXTENSION hstore; CREATE EXTENSION pgrouting;'
osm2pgsql -c -k -E 3035 $1
# SQL script for creating pgrouting table with segmented roads
psql -f pgrouting_setup.sql
