#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

#SETUP
sudo apt install postgresql-10 postgis proj-bin postgresql-10-postgis-scripts postgresql-10-postgis-2.4* osm2pgsql openjdk-11-jdk openjdk-11-jre maven
