#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

if [[ $# -eq 0 ]]; then
    echo "Usage: ./RUN.sh file.osm"
    exit 1
fi


echo "Setting up PostgreSQL and PostGIS"
./setup.sh
echo "Importing OSM file"
./import_db.sh $1
./process_db.sh
