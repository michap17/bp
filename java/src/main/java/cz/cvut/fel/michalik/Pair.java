package cz.cvut.fel.michalik;

/**
 * Class used in NearbyCollector.countAll().
 * Holds result of matched row by NearbyCollector.testWeightedKeys().
 */
public class Pair {
    public final double weight;
    public final String match;

    public Pair(double weight, String match) {
        this.weight = weight;
        this.match = match;
    }

    public double getWeight() {
        return weight;
    }

    public String getMatch() {
        return match;
    }
}
