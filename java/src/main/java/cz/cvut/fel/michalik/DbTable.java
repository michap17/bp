package cz.cvut.fel.michalik;

/**
 * Database tables imported by osm2pgsql
 */
public enum DbTable {
    READ_POINT ("planet_osm_point","point"),
    READ_LINE ("planet_osm_line","line"),
    READ_WAYS ("planet_osm_ways_noded","ways"),
    READ_POLY ("planet_osm_polygon","poly");
    
    private final String tableName;
    private final String shortName;
    DbTable(String tableName, String shortName){
        this.tableName = tableName;
        this.shortName = shortName;
    }

    @Override
    public String toString() {
        return tableName;
    }

    public String getShortName() {
        return shortName;
    }
}
