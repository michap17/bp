package cz.cvut.fel.michalik;

import org.geotools.geojson.geom.GeometryJSON;
import org.geotools.geometry.jts.JTS;
import org.geotools.referencing.CRS;
import org.locationtech.jts.geom.*;
import org.locationtech.jts.operation.buffer.BufferParameters;
import org.locationtech.jts.operation.buffer.OffsetCurveBuilder;
import org.locationtech.jts.operation.distance.DistanceOp;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.MathTransform;
import org.opengis.referencing.operation.TransformException;
import org.postgis.PGgeometry;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Static class with useful functions
 */
public class Utils {

    private static final int SRID = (int) Properties.getSettings().get("SRID");
    private static GeometryFactory geometryFactory = null;

    /**
     * Translates geometry into GeoJSON
     * @param conn DB connection
     * @param geo PGgeometry
     * @return GeoJSON string
     */
    public static String geoToString(Connection conn, PGgeometry geo){
        try {
            PreparedStatement ps_geoToString = conn.prepareStatement("SELECT ST_AsGeoJSON(ST_Transform(?,4326))");
            ps_geoToString.setObject(1, geo);
            ResultSet res = ps_geoToString.executeQuery();
            res.next();
            return res.getString(1);
        } catch (SQLException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, null, ex);
            return "";
        }
    }

    /**
     * Translates geometry into GeoJSON
     * @param geo Geometry
     * @return GeoJSON string
     */
    public static String geoToGeoJSON(Geometry geo) {
        Geometry geometry = projectBack(geo);
        return new GeometryJSON((Integer) Properties.getSettings().get("DEGREES_PRECISION")).toString(geometry);
    }

    /**
     * Translates geometry back into WGS 84 CRS
     * using GeoTools
     * @param geo Geometry
     * @return GeoJSON string
     */
    public static Geometry projectBack(Geometry geo){
        Geometry geoCopy = geo.copy();
        geoCopy.apply(new ProjectBack());
        return geoCopy;
    }

    /**
     * Returns distance between 2 geometries
     * using JTS library
     * @param obj1 First geometry
     * @param obj2 Second geometry
     * @return Distance as double
     */
    public static double dist_jts(PGgeometry obj1, PGgeometry obj2){
        Geometry g1 = PGtoJTS(obj1);
        Geometry g2 = PGtoJTS(obj2);
        return g1.distance(g2);
    }

    /**
     * Returns length of given geometry
     * using JTS library
     * @param obj1 Geometry
     * @return Length as double
     */
    public static double length_jts(PGgeometry obj1){
        Geometry geometry = PGtoJTS(obj1);
        return geometry.getLength();
    }

    public static PGgeometry getSideLine(Connection conn, PGgeometry road, double distance){
        try {
            PreparedStatement ps_sideLine = conn.prepareStatement("SELECT ST_OffsetCurve(?, ?)");
            ps_sideLine.setObject(1, road);
            ps_sideLine.setDouble(2, distance);
            ResultSet rs = ps_sideLine.executeQuery();
            rs.next();
            return rs.getObject(1, PGgeometry.class);
        } catch (SQLException ex) {
            Logger.getLogger(Utils.class.getName()).log(Level.SEVERE, "Failed getting polygon to the side of a road", ex);
        }
        return null;
    }

    /**
     * Gets line that is distance meters away from road
     * @param road Road geometry
     * @param distance Distance (can be negative)
     * @return Returns Geometry (in most cases LineString) with given offset from road
     */
    public static Geometry getSideLine_jts(Geometry road, double distance){
        BufferParameters bufParams = new BufferParameters();
        bufParams.setSingleSided(true);
        OffsetCurveBuilder builder = new OffsetCurveBuilder(road.getPrecisionModel(),bufParams);
        Coordinate[] offsetCoordinates = builder.getOffsetCurve(road.getCoordinates(),distance);
        GeometryFactory gf = Utils.getGeometryFactory(null);
        if(offsetCoordinates.length == 1){
            return gf.createPoint(offsetCoordinates[0]);
        }else {
            return gf.createLineString(offsetCoordinates);
        }
    }

    /**
     * Returns GeometryFactory for creating geometries
     * @param obj PGgeometry for getting SRID (if null, SRID is used)
     * @return GeometryFactory
     */
    public static GeometryFactory getGeometryFactory(PGgeometry obj){
        if(geometryFactory != null) return geometryFactory;
        int srid = obj == null ? SRID : obj.getGeometry().srid;
        geometryFactory = new GeometryFactory(new PrecisionModel(),srid);
        return geometryFactory;
    }

    /**
     * Transforms PGgeometry to JTS geometry
     * @param obj PGgeometry object
     * @return Geometry
     */
    public static Geometry PGtoJTS(PGgeometry obj){
        GeometryFactory factory = getGeometryFactory(obj);
        return PGtoJTS(obj.getGeometry(),factory);
    }

    private static Geometry PGtoJTS(org.postgis.Geometry geometry, GeometryFactory factory){
        Geometry geometryResult;
        switch(geometry.getType()){
            case 0:
                System.out.println(geometry.getTypeString());
                geometryResult = factory.createLinearRing(PGtoCoordinates(geometry));
                break;
            case 1:
                geometryResult = factory.createPoint(PGtoCoordinates(geometry)[0]);
                break;
            case 2:
                geometryResult = factory.createLineString(PGtoCoordinates(geometry));
                break;
            case 3:
                org.postgis.Polygon polygon = (org.postgis.Polygon) geometry;
                LinearRing shell = null;
                LinearRing[] rings = new LinearRing[polygon.numRings()-1];
                for(int i = 0; i < polygon.numRings(); i++){
                    Coordinate[] coordinates = PGtoCoordinates(polygon.getRing(i));
                    if(i == 0){
                        shell = factory.createLinearRing(coordinates);
                    }else {
                        rings[i-1] = factory.createLinearRing(coordinates);
                    }
                }
                geometryResult = factory.createPolygon(shell, rings);
                break;
            case 4:
                org.postgis.MultiPoint multiPoint = (org.postgis.MultiPoint) geometry;
                Point[] points = new Point[multiPoint.numGeoms()];
                for(int i = 0; i < multiPoint.numGeoms(); i++){
                    points[i] = (Point) PGtoJTS(multiPoint.getSubGeometry(i),factory);
                }
                geometryResult = factory.createMultiPoint(points);
                break;
            case 5:
                org.postgis.MultiLineString multiLine = (org.postgis.MultiLineString) geometry;
                LineString[] lines = new LineString[multiLine.numLines()];
                for(int i = 0; i < multiLine.numLines(); i++){
                    lines[i] = (LineString) PGtoJTS(multiLine.getSubGeometry(i),factory);
                }
                geometryResult = factory.createMultiLineString(lines);
                break;
            case 6:
                org.postgis.MultiPolygon multiPolygon = (org.postgis.MultiPolygon) geometry;
                Polygon[] polygons = new Polygon[multiPolygon.numPolygons()];
                for(int i = 0; i < multiPolygon.numPolygons(); i++){
                    polygons[i] = (Polygon) PGtoJTS(multiPolygon.getSubGeometry(i),factory);
                }
                geometryResult = factory.createMultiPolygon(polygons);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + geometry.getTypeString());
        }
        return geometryResult;
    }

    private static Coordinate[] PGtoCoordinates(org.postgis.Geometry geometry){
        Coordinate[] coords = new Coordinate[geometry.numPoints()];
        for(int i = 0; i < geometry.numPoints(); i++){
            org.postgis.Point point = geometry.getPoint(i);
            coords[i] = new Coordinate(point.x,point.y);
        }
        return coords;
    }

    /**
     * For given OSM ID, finds name of
     * that road
     * @param osmId ID
     * @return Name of street
     */
    public static String getStreetName(Connection conn, long osmId) throws SQLException {
        PreparedStatement nextWays = conn.prepareStatement(
                "SELECT name FROM planet_osm_line WHERE osm_id = ?");
        nextWays.setLong(1,osmId);
        ResultSet resultSet = nextWays.executeQuery();
        resultSet.next();
        return resultSet.getString("name");
    }

    private static class ProjectBack implements CoordinateFilter{
        private static MathTransform transform = null;
        @Override
        public void filter(Coordinate coordinate) {
            try {
                if(transform == null){
                    CoordinateReferenceSystem source = CRS.decode("EPSG:" + SRID);
                    CoordinateReferenceSystem target = CRS.decode("EPSG:4326");
                    transform = CRS.findMathTransform(source, target);
                }
                /*
                 Swapping coordinates intentionally,
                 because coordinate transformation back to EPSG:4326 without it wouldn't work.
                 This won't affect any measurements.
                 */
                Coordinate projected = JTS.transform(new Coordinate(coordinate.y,coordinate.x),null, transform);
                coordinate.x = projected.y;
                coordinate.y = projected.x;
            } catch (FactoryException | TransformException e) {
                e.printStackTrace();
            }
        }
    }
}
