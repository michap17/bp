package cz.cvut.fel.michalik.feature;

import java.util.List;
import java.util.Map;

/**
 * Class used for representing a single category (green, roads, water)
 */
public class Query {
    // Key value pairs with weights
    private final Map<Double,Map<String,List<String>>> weighted_keyvals;
    // Search distance for nearby objects for this query
    private final Double distance;

    public Query(Map<Double,Map<String, List<String>>> weighted_keyvals, Double distance) {
        this.weighted_keyvals = weighted_keyvals;
        this.distance = distance;        
    }

    public Map<Double,Map<String,List<String>>> getWeightedKeyvals() {
        return weighted_keyvals;
    }

    public Double getDistance() {
        return distance;
    }
    
}
