package cz.cvut.fel.michalik;

import me.tongfei.progressbar.ProgressBar;
import me.tongfei.progressbar.ProgressBarStyle;

/**
 * Wrapper class for library
 * https://github.com/ctongfei/progressbar
 */
public class Progress {
    private static ProgressBar bar = null;

    /**
     * Initialize and start progress bar
     * @param total Number of roads
     */
    public static void init(int total){
        bar = new ProgressBar("Processing: ", total, 1000,System.out,ProgressBarStyle.ASCII,"",1);
    }

    /**
     * Called after completion of processing 1 road
     */
    public static synchronized void step(){
        if(bar == null) return;
        bar.step();
        bar.setExtraMessage(Runtime.getRuntime().totalMemory() / 1000000 + " / " + Runtime.getRuntime().maxMemory() / 1000000 + " MB");
    }

    /**
     * Stop progress bar
     */
    public static void close(){
        if(bar == null) return;
        bar.close();
    }


}
