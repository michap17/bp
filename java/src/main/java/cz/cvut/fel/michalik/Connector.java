package cz.cvut.fel.michalik;

import org.apache.commons.dbcp2.*;
import org.apache.commons.pool2.impl.GenericObjectPool;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class connects to PostgreSQL database
 * with given credentials
 */
public class Connector {

    private static PoolingDataSource<PoolableConnection> source = null;

    /**
     * Connect to database and get connection.
     * @return Connection
     */
    protected static Connection connect() {
        try {
            if(source == null) setupDataSource();
            return source.getConnection();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            return null;
        }
    }

    /**
     * Setup connection pool.
     */
    private static void setupDataSource(){
        ConnectionFactory connectionFactory = new DriverManagerConnectionFactory(
                (String) Properties.getSettings().get("DB_URL"),
                (String) Properties.getSettings().get("DB_NAME"),
                (String) Properties.getSettings().get("DB_PASS"));
        PoolableConnectionFactory poolableConnectionFactory = new PoolableConnectionFactory(connectionFactory,null);
        GenericObjectPool<PoolableConnection> connectionPool = new GenericObjectPool<>(poolableConnectionFactory);
        connectionPool.setMaxTotal((int) Properties.getSettings().get("DB_CONNS"));
        poolableConnectionFactory.setPool(connectionPool);
        System.out.println("Connected to database");
        source = new PoolingDataSource<>(connectionPool);
    }

    /**
     * Close connection.
     * This actually returns connection to pool
     * for reusability.
     * @param conn Closed connection
     */
    public static void closeConnection(Connection conn) {
        try {
            conn.close();
        } catch (SQLException e) {
            Logger.getLogger(Connector.class.getName()).log(Level.SEVERE,"Could not close connection", e);
        }
    }
}
