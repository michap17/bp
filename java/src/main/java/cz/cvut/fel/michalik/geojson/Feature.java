package cz.cvut.fel.michalik.geojson;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import cz.cvut.fel.michalik.NearbyCollector;
import cz.cvut.fel.michalik.Road;
import cz.cvut.fel.michalik.feature.Query;

import java.util.List;
import java.util.Map;


/**
 * GeoJSON feature
 * GSON library creates JSON from this class instance
 */
public class Feature {
    public final String type = "Feature";
    // Created by ST_AsGeoJSON
    public final JsonElement geometry;
    // Properties as Map
    public final Map<String, Object> properties;
    // OSM ID
    public long id;

    public Feature(String geometry, Map<String, Object> properties) {
        this.geometry = new Gson().fromJson(geometry, JsonElement.class);
        this.properties = properties;
    }
    
    public Feature(String geometry, Map<String, Object> properties, Road road) {
        this(geometry, properties);
        this.id = road.getID();
        this.properties.put("_osm",road.getOsmID());
        this.properties.put("_segment_count", (double) road.getSegments().size());
    }

    public Feature(String geometry, Map<String, Object> properties, Road road, List<Map<String, Object>> poi) {
        this(geometry,properties, road);
        properties.put("_poi", poi);
    }

    public Feature(NearbyCollector nc, Map<String, Query> properties){
        this(nc.getRoad().roadToGeoJSON(), nc.mapToValues(properties), nc.getRoad(), nc.collectPOI());
        nc.close();
    }
    
}
