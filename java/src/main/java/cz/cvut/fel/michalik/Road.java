package cz.cvut.fel.michalik;

import cz.cvut.fel.michalik.feature.Query;
import org.locationtech.jts.geom.*;
import org.locationtech.jts.linearref.LengthIndexedLine;
import org.locationtech.jts.operation.distance.DistanceOp;
import org.postgis.PGgeometry;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class holding selected way (road) and its segments.
 */
public class Road {

    // DB connection
    private final Connection conn;
    // Road geometry
    private final PGgeometry road;
    // JTS geometry
    private final LineString roadGeometry;
    // ID of road
    private final long road_id;
    // OSM ID of road
    private long osm_id;
    // List of road segments
    private List<RoadSegment> segments;
    // Length of one segment
    private final double SEG_LEN = (double) Properties.getSettings().get("SEG_LEN");
    // Distance between original and offset road
    private final double OFFSET = (double) Properties.getSettings().get("OFFSET");
    // Left and right sides of road
    private Map<Double,PGgeometry> road_sides;
    public boolean throughRoad;

    /**
     * Creates new Road
     * @param conn DB connection
     * @param roadID OSM id
     * @param table Table with roads (planet_osm_lines or planet_osm_ways_noded)
     */
    public Road(Connection conn, long roadID, DbTable table){
        this.conn = conn;
        this.road_id = roadID;
        this.road = roadIDtoGeometry(road_id, table);
        // Convert road to JTS geometry
        this.roadGeometry = (LineString) Utils.PGtoJTS(road);
        // Divide road into segments
        this.segments = segmentRoad_jts();
        this.throughRoad = false;
    }

    /**
     * Transforms road geometry to GeoJSON
     *
     * @return GeoJSON string
     */
    public String roadToGeoJSON() {
        return Utils.geoToGeoJSON(roadGeometry);
        //return Utils.geoToString(conn, road);
    }

    public PGgeometry getRoadGeometry() {
        return road;
    }

    public long getID() {
        return this.road_id;
    }

    public long getOsmID() {
        return this.osm_id;
    }

    public List<RoadSegment> getSegments() {
        return segments;
    }

    /**
     * For given OSM ID of road finds its geometry representation
     *
     * @param id OSM ID
     * @return Geometry
     */
    public PGgeometry roadIDtoGeometry(long id, DbTable table) {
        try {
            Statement stat = conn.createStatement();
            ResultSet rs;
            if(table.getShortName().equals("ways")) {
                // Used in Main
                rs = stat.executeQuery("SELECT old_id,way FROM " + table + " WHERE id=" + id);
                rs.next();
                this.osm_id = rs.getLong("old_id");
            }else{
                // Used in testing
                rs = stat.executeQuery("SELECT way FROM " + table + " WHERE osm_id=" + id);
                rs.next();
                this.osm_id = id;
            }
            return rs.getObject("way", PGgeometry.class);
        } catch (SQLException ex) {
            Logger.getLogger(Road.class.getName()).log(Level.SEVERE, "Invalid road ID", ex);
            return null;
        }
    }

    /**
     * From road create segments of this road
     * using JTS library
     * @return List with segments as RoadSegments
     */
    private List<RoadSegment> segmentRoad_jts(){
        segments = new LinkedList<>();
        double roadLen = Utils.length_jts(road);
        int segCount = (int) Math.ceil(roadLen / SEG_LEN);
        // Segment count is limited to maximum of 100 segments
        segCount = Math.min(segCount,100);
        // Segments are created by indexing using length indexes
        LengthIndexedLine indexedLine = new LengthIndexedLine(this.roadGeometry);
        for(double seg = 0; seg < segCount; seg++){
            LineString segLine = (LineString) indexedLine.extractLine((seg/segCount)*roadLen,((seg+1)/segCount)*roadLen);
            segments.add(new RoadSegment(segLine, true));
        }
        return segments;
    }

    /**
     * For given DB row, process object at this row
     * @param rs DB row
     * @param q Query
     * @param weight Object weight
     */
    public void processObject(ResultSet rs, String key, Query q, double weight) {
        Geometry targetGeometry = null;
        long targetID = 0;
        try {
            targetGeometry = Utils.PGtoJTS(rs.getObject("way", PGgeometry.class));
            targetID = rs.getLong("osm_id");
        } catch (SQLException ex) {
            Logger.getLogger(Road.class.getName()).log(Level.SEVERE, "Failed to get column 'way'", ex);
        }
        //if(targetID != 304425577) return;
        // Update every segment
        for(int i = 0; i < segments.size(); i++){
            List<RelativePosition> pos = new LinkedList<>();
            RoadSegment segment = segments.get(i);
            // Get 2 closest points from segment and target geometries
            DistanceOp distanceOp = new DistanceOp(segment.getSegmentGeometry(),targetGeometry);
            // Shortest line is defined by 2 closest points
            Geometry line = Utils.getGeometryFactory(null).createLineString(distanceOp.nearestPoints());
            // Distance between 2 closest points
            double distance = distanceOp.distance();
            // Same road might be selected as nearby object
            if(road_id == targetID) {
                pos.add(RelativePosition.POS);
                pos.add(RelativePosition.NEG);
                this.throughRoad = true;
            }else{
                // Segment is too far away, skip it
                if (distance > q.getDistance()) {
                    continue;
                }
                // If target object is to the left or right of this segment
                pos = getRelativePositions(segment, targetGeometry, 0.1);
            }
            // Update value in each segment
            segment.addValue(line, pos, weight,distance);
        }
        // Detect way going through any road
        if(key.equals("roads")) {
            // Road might be split in intersection, so crosses() function won't help
            boolean crosses = roadGeometry.intersects(targetGeometry)
                    && !roadGeometry.getStartPoint().intersects(targetGeometry)
                    && !roadGeometry.getEndPoint().intersects(targetGeometry);
            if (crosses) {
                this.throughRoad = true;
            }
        }
    }

    /**
     * For given DB row, process obstacle at this row
     * @param rs DB row
     */
    public void processObstacle(ResultSet rs){
        try {
            PGgeometry targetGeometry = rs.getObject("way", PGgeometry.class);
            double dist = Utils.dist_jts(road, targetGeometry);
            segments.forEach(roadSegment -> roadSegment.updateObstacles(targetGeometry, dist));
        } catch (SQLException ex) {
            Logger.getLogger(Road.class.getName()).log(Level.SEVERE, "Failed to get column 'way'", ex);
        }
    }

    /**
     * Find relative position for 2 geometries
     * @param roadSegment RoadSegment
     * @param targetGeometry Target geometry
     * @param epsilon Relative tolerance for comparing distances
     * @return RelativePosition
     */
    public List<RelativePosition> getRelativePositions(RoadSegment roadSegment, Geometry targetGeometry, double epsilon) {
        double distRef = roadSegment.getSegmentGeometry().distance(targetGeometry);
        List<RelativePosition> positions = new LinkedList<>();
        // Tolerance for comparing distance changes
        double EPSILON = OFFSET * epsilon;
        for(Map.Entry<RelativePosition, Geometry> entry : roadSegment.getSegment_sides().entrySet()){
            double dist = entry.getValue().distance(targetGeometry);
            // If target gets closer to this side or both distances are zero
            if(dist - EPSILON < distRef || (distRef == 0 && dist == 0)){
                positions.add(entry.getKey());
            }
        }
        return positions;
    }

    /**
     * Calculate how many segments are satisfied for given Query
     * on just one road side (going by)
     * @return Number in range [0,1]
     */
    public double calculateGoingBy(){
        // Average of all segment values on both sides
        return segments.stream()
                .mapToDouble(RoadSegment::goingBy)
                .sum() / (double) segments.size();
    }

    /**
     * Calculate how many segments are satisfied for given Query
     * on both road sides.
     * @return Number in range [0,1]
     */
    public double calculateGoingThrough(){
        return segments.stream()
                .mapToDouble(RoadSegment::goingThrough)
                .sum() / (double) segments.size();
    }

    /**
     * Reset segment values for new Query
     * @param init_value Initial value for segments
     */
    public void initSegmentValues(Double init_value) {
        segments.forEach(seg -> seg.initValues(init_value));
    }

    /**
     * Removes objects that are obstructed by some obstacle
     */
    public void removeObstructed(){segments.forEach(RoadSegment::removeObstructed);}

    public LineString getRoadJTSGeometry() {
        return this.roadGeometry;
    }
}
