package cz.cvut.fel.michalik;

import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.linearref.LengthIndexedLine;
import org.postgis.PGgeometry;

import java.sql.Connection;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Class holding part of road, its nearby objects and obstacles.
 */
public class RoadSegment {

    private final LineString segment;
    // For every nearby object a Value is created, that contains its weight and 'sight' line
    private Map<RelativePosition, List<Value>> values;
    // List of obstacles
    private List<Value> obstacles;
    // Distance between original and offset road
    private final double OFFSET = (double) Properties.getSettings().get("OFFSET");
    // Left and right sides of road
    private final Map<RelativePosition,Geometry> segment_sides;

    /**
     * Creates new RoadSegment from given Geometry
     * @param segment Geometry
     * @param cutSides Trim sides using cutSegments function
     */
    public RoadSegment(LineString segment, boolean cutSides) {
        this.segment = segment;
        this.segment_sides = new HashMap<>();
        Connection conn = Connector.connect();
        if(cutSides) {
            segment_sides.put(RelativePosition.POS, cutSegment(Utils.getSideLine_jts(this.segment, OFFSET)));
            segment_sides.put(RelativePosition.NEG, cutSegment(Utils.getSideLine_jts(this.segment, -OFFSET)));
        }else{
            segment_sides.put(RelativePosition.POS, Utils.getSideLine_jts(this.segment, OFFSET));
            segment_sides.put(RelativePosition.NEG, Utils.getSideLine_jts(this.segment, -OFFSET));
        }
        Connector.closeConnection(conn);
        initValues(Double.NEGATIVE_INFINITY);
    }

    /**
     * LineString segment is shortened by OFFSET/2 distance on each end.
     * Start and end point are then used in segment_sides.
     * If segment is shorter than OFFSET, centroid of this line is returned.
     * @param geometry Original segment
     * @return Shortened segment
     */
    private Geometry cutSegment(Geometry geometry){
        if(geometry.getNumPoints() == 1) return geometry;
        LineString segment = (LineString) geometry;
        double length = segment.getLength();
        if(length <= OFFSET){
            // Shortening not possible, returns centroid
            return segment.getCentroid();
        }
        LengthIndexedLine indexedLine = new LengthIndexedLine(segment);
        return indexedLine.extractLine(OFFSET/2,length - OFFSET/2);
    }

    /**
     * Gets geometries that are to the sides from this segment.
     * @return Map with geometries, keys are relative positions
     */
    public Map<RelativePosition, Geometry> getSegment_sides() {
        return segment_sides;
    }

    public Geometry getSegmentGeometry() {
        return segment;
    }

    /**
     * Remembers this obstacle for future use
     * @param obstacle Obstacle geometry
     * @param dist Distance from this segment
     */
    public void updateObstacles(PGgeometry obstacle, double dist){
        obstacles.add(new Value(0.0, Utils.PGtoJTS(obstacle), dist));
    }

    /**
     * Updates weights for each side
     * @param min_line Line connecting 2 closest points
     * @param positions RelativePosition
     * @param weight Object weight
     * @param dist Distance between 2 closest points (min_line length)
     */
    public void addValue(Geometry min_line, List<RelativePosition> positions, double weight, double dist) {
        for(RelativePosition position : positions){
            this.values.get(position).add(new Value(weight, min_line, dist));
        }
    }

    /**
     * For each side removes objects that are not visible
     */
    public void removeObstructed(){
        values.replaceAll((k, v) -> removeObstructed(v));
    }

    /**
     * Removes all objects that are not visible for given
     * list of object on one road side
     * @param values List of objects
     * @return Filtered List
     */
    private List<Value> removeObstructed(List<Value> values){
        for(Value obstacleValue : obstacles){
            values = values.stream()
                    .filter(value ->
                            obstacleValue.getDistance() > value.getDistance() ||
                                    obstacleValue.getGeom().distance(value.getGeom()) > 0.001)
                    .collect(Collectors.toList());
        }
        return values;
    }

    /**
     * Finds best weight among given list of values
     * @param values List of values
     * @return Best value
     */
    private Double getBestWeight(List<Value> values){
        OptionalDouble max = values.stream().mapToDouble(Value::getWeight).max();
        if(max.isPresent()){
            return max.getAsDouble();
        }else{
            return 0.0;
        }
    }

    /**
     * Returns best value on given position
     * @param position Relative position
     * @return Value
     */
    public double getBestValue(RelativePosition position){
        return getBestWeight(values.get(position));
    }

    /**
     * Calculates value for subcategory "through" or "between"
     * @return Value
     */
    public double goingThrough() {
        if(getBestValue(RelativePosition.POS) == getBestValue(RelativePosition.NEG)) {
            return getBestValue(RelativePosition.POS);
        }else{
            return 0.0;
        }
    }

    /**
     * Calculates value for subcategory "by"
     * @return Value
     */
    public double goingBy() {
        if(goingThrough() == 0.0){
            double value = 0.0;
            for(RelativePosition position : segment_sides.keySet()){
                value = Double.max(value, getBestWeight(values.get(position)));
            }
            return value;
        }else{
            return 0.0;
        }
    }

    /**
     * Initializes all lists of values
     * @param init_value Initial value
     */
    public void initValues(Double init_value) {
        values = new HashMap<>();
        for(RelativePosition position : RelativePosition.values()) {
            values.put(position, new LinkedList<>());
        }
        this.obstacles = new LinkedList<>();
    }

    public double getOFFSET() {
        return OFFSET;
    }

    /**
     * Inner class for storing found objects and obstacles
     */
    private static class Value {
        private final Double weight;
        private final Geometry geometry;
        private final Double distance;

        public Value(Double weight, Geometry geometry, Double distance) {
            this.weight = weight;
            this.geometry = geometry;
            this.distance = distance;
        }

        public Double getWeight() {
            return weight;
        }

        public Geometry getGeom() {
            return geometry;
        }

        public Double getDistance() {
            return distance;
        }
    }
}
