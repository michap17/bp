package cz.cvut.fel.michalik;

/**
 * Possible positions of nearby objects relative to given way.
 */
public enum RelativePosition {
    POS("vlevo"),
    NEG("vpravo");

    private final String name;
    RelativePosition(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static RelativePosition negate(RelativePosition position) {
        switch(position){
            case POS: return NEG;
            case NEG: return POS;
            default: return null;
        }
    }
}
