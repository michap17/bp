package cz.cvut.fel.michalik;

import cz.cvut.fel.michalik.feature.Query;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.util.PointExtracter;
import org.postgis.PGgeometry;

import java.sql.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * For given road geometry collects all nearby objects and computes their
 * property values
 *
 * @author petr
 */
public class NearbyCollector implements AutoCloseable{

    private final Connection conn;
    // Distance in meters
    private final double DISTANCE;
    // Road object
    private final Road road;
    // ResultSets with nearby objects
    private Map<DbTable,ResultSet> resultMap = null;

    /**
     * Creates collector of objects near given road
     * @param roadID Road OSM ID
     * @param dist Distance in meters
     * @param table Database table with referenced road ID
     */
    public NearbyCollector(long roadID, double dist, DbTable table) {
        this.conn = Connector.connect();
        this.road = new Road(conn, roadID, table);
        this.DISTANCE = dist;
    }

    public Road getRoad() {
        return road;
    }

    /**
     * Queries database and finds all nearby objects. Objects are searched in
     * given list of tables. If list of tables is null, searching in tables with
     * points, lines and polygons.
     *
     * @param tables Database tables
     * @return this
     */
    public NearbyCollector collectAll(List<DbTable> tables) {
        this.resultMap = new HashMap<>();
        if (tables == null) {
            tables = new LinkedList<>(Arrays.asList(DbTable.READ_POINT, DbTable.READ_LINE, DbTable.READ_POLY));
        }        
        for (DbTable table : tables) {
            try{
                // PreparedStatement is scrollable for reusability
                PreparedStatement ps = conn.prepareStatement("SELECT near.* "
                                 + "FROM " + table.toString() + " AS near "
                                 + "WHERE ST_DWithin(?, near.way,?) "
                                 + "AND (tunnel IS NULL OR tunnel NOT IN ('yes','culvert','flooded'))",
                        ResultSet.TYPE_SCROLL_INSENSITIVE,
                        ResultSet.CONCUR_READ_ONLY);
                ps.setObject(1, road.getRoadGeometry());
                ps.setDouble(2, DISTANCE);
                ResultSet resultSet = ps.executeQuery();
                // After closing ResultSet, PreparedStatement closes too
                ps.closeOnCompletion();
                this.resultMap.put(table, resultSet);
            } catch (SQLException ex) {
                Logger.getLogger(NearbyCollector.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return this;
    }

    /**
     * Maps multiple categories with key value pairs to their respective values.
     * See countAll().
     *
     * @param queryMap Map of keywords to Query (key value pairs)
     * @return Map of counts
     */
    public Map<String, Object> mapToValues(Map<String, Query> queryMap) {
        Map<String, Object> res = new HashMap<>();
        queryMap.forEach((key, value) -> res.putAll(this.countAll(key, value)));
        return res;
    }

    /**
     * For a given single Query, computes values for its subcategories.
     * Example:
     * Keyword "green" is mapped to ("green_by"=0.5,"green_through"=0.1)
     *
     * @param key Name of category (keyword)
     * @param query String of key and values.
     * @return Map of subcategories with values
     */
    private Map<String, Object> countAll(String key, Query query) {
        if (resultMap == null) {
            return null;
        }
        Map<String, Object> result = new HashMap<>();
        // Initialize values for each road segment
        road.initSegmentValues(0.0);
        // Matched objects as string
        StringBuilder matched = new StringBuilder();
        // List of obstacles
        Map<String, List<String>> obstaclesList = Properties.getObstacles();
        // Try every row (object)
        for (ResultSet rs : resultMap.values()) {
            try {
                while (rs.next()) {
                    // Filter out only relevant objects
                    Pair filtered = testWeightedKeys(rs, query.getWeightedKeyvals());
                    // Object is not in given category, but it might be an obstacle
                    if(filtered == null){
                        String obstacle = testKeys(rs, obstaclesList);
                        if(obstacle != null){
                            road.processObstacle(rs);
                        }
                    }else{
                        // String of matched objects is created for every category
                        // Add key-value pair to string, if not already present
                        int idx = matched.indexOf(filtered.getMatch());
                        if(idx == -1){
                            matched.append(filtered.getMatch()).append(";");
                        }
                        // And process given object
                        road.processObject(rs,key,query,filtered.getWeight());
                        /*
                        if(road.getID() == 42099283L && key.equals("roads")){
                            long id = rs.getLong("osm_id");
                            System.out.printf("[DEBUG] %d\n",id);
                        }
                        */

                    }
                }
                // Reset ResultSet
                rs.beforeFirst();
            } catch (SQLException ex) {
                Logger.getLogger(NearbyCollector.class.getName()).log(Level.SEVERE, "Failed processing object", ex);
            }
        }
        //Finalize road segments
        road.removeObstructed();
        result.put(key + "_by", road.calculateGoingBy());
        // Way can go through park, between roads or between lakes
        if(key.equals("green")) {
            result.put(key + "_through", road.calculateGoingThrough());
        } else if(key.equals("roads")) {
            // Roads are split into "through" and "between"
            result.put(key + "_between", (road.throughRoad ? 0 : 1) * road.calculateGoingThrough());
            result.put(key + "_through", (road.throughRoad ? 1 : 0) * road.calculateGoingThrough());
        }else {
            result.put(key + "_between", road.calculateGoingThrough());
        }
        result.put("_" + key + "_matched", matched.toString());
        //System.out.println(result);
        return result;
    }

    /**
     * Tests every key-value with weights for given row.
     * If multiple weights are satisfied, returns highest
     * weight with found key value as String.
     * Values are returned as Pair object.
     * @param rs ResultSet with loaded row
     * @param weightedKeyVals List with weighted strings of key value pairs
     * @return Map with highest weight and String
     */
    private Pair testWeightedKeys(ResultSet rs, Map<Double,Map<String,List<String>>> weightedKeyVals) {
        Pair result = null;
        for(Map.Entry<Double, Map<String,List<String>>> entry : weightedKeyVals.entrySet()){
            double weight = entry.getKey();
            String match = testKeys(rs,entry.getValue());
            if(match != null && (result == null || result.getWeight() < weight)){
                result = new Pair(weight,match);
            }
        }
        return result;
    }

    /**
     * Tests if at least one key value pair is in given row.
     * Key is column in table and value is text in that column. 
     * Keyvalue pair example: "key1=value1,value2,value3;key2=value4;key3=*"
     *
     * @param rs ResultSet with loaded row
     * @param keyVals Map of key value pairs
     * @return String with matched string or null
     */
    private String testKeys(ResultSet rs, Map<String,List<String>> keyVals) {
        var contained = keyVals.entrySet().stream()
                .map(entry -> testKeyVal(rs,entry.getKey(),entry.getValue()))
                .filter(val -> val != null)
                .findFirst();
        return contained.orElse(null);
    }

    /**
     * For one row and one key tests if given key contains one of given values.
     * Value can be * - this means, that value can be anything.
     *
     * @param rs ResultSet with loaded row
     * @param key Key String
     * @param values List of String values
     * @return String with satisfied key value
     */
    private String testKeyVal(ResultSet rs, String key, List<String> values) {
        String column_val;
        try {
            column_val = rs.getString(key);
            // If null, value is missing
            if (rs.wasNull()) {
                return null;
            }
        } catch (SQLException ex) {
            // Key might be in column 'tags' as hstore
            try {
                Map<String,String> tags_val = (Map<String, String>) rs.getObject("tags");
                // Test if present
                if(!tags_val.containsKey(key)){
                    return  null;
                }else{
                    column_val = tags_val.get(key);
                }
            } catch (SQLException ex2) {
                Logger.getLogger(NearbyCollector.class.getName()).log(Level.SEVERE, "Problem when testing key value pairs with ResultSet", ex2);
                return null;
            }
        }
        // Column contains wildcard or some defined value
        if(values.contains("*") || values.contains(column_val)){
            return key + "=" + column_val;
        }else{
            return null;
        }
    }

    /**
     * Add points of interest (POI) for every way.
     * @return Map of POI for set points on the way
     */
    public List<Map<String, Object>> collectPOI(){
        if (resultMap == null) {
            return null;
        }
        // Search distance
        double POI_DIST = (double) Properties.getSettings().get("DISTANCE");
        // Minimal distance for each found intersection
        Map<Geometry, Double> pointDist = new HashMap<>();
        // Detail (name and type) of nearest POI for each nearest intersection
        Map<Geometry, Map<String, String>> poi = new HashMap<>();
        // List of possible points of interest
        Map<String,List<String>> poiList = Properties.getPOIs();
        /*
        Detect intersections
        (way might be connected to multiple toher ways)
        */
        // Map with intersection geometries mapped to IDs of next ways
        Map<Geometry, Map<Long, String>> intersections = new HashMap<>();
        // Get street names
        Map<Long, String> streetName = new HashMap<>();
        LineString roadGeometry = road.getRoadJTSGeometry();
        try {
            // Get start and end node IDs of given way
            Statement wayNodes = conn.createStatement();
            ResultSet wayNodesRes = wayNodes.executeQuery(
                    "SELECT source, target FROM planet_osm_ways_noded WHERE id = " + road.getID()
            );
            wayNodesRes.next();
            long nodeStartId = wayNodesRes.getLong("source");
            long nodeEndId = wayNodesRes.getLong("target");
            // Get all subsequent way
            PreparedStatement nextWays = conn.prepareStatement(
                    "SELECT * FROM planet_osm_ways_noded WHERE " +
                        "(source IN (?,?) OR target IN (?,?)) AND id <> ?");
            nextWays.setLong(1,nodeStartId);
            nextWays.setLong(2,nodeEndId);
            nextWays.setLong(3,nodeStartId);
            nextWays.setLong(4,nodeEndId);
            nextWays.setLong(5, road.getID());
            ResultSet resultSet = nextWays.executeQuery();
            // Remember every subsequent way and generate simple instruction
            while (resultSet.next()) {
                long targetId = resultSet.getLong("id");
                long osmId = resultSet.getLong("old_id");
                Geometry targetGeometry = Utils.PGtoJTS(resultSet.getObject("way", PGgeometry.class));
                String name = Utils.getStreetName(conn, osmId);
                Geometry intersection = roadGeometry.intersection(targetGeometry);
                List<Point> points = PointExtracter.getPoints(intersection);
                // Roads might have more than 1 point in common (but it should be a rare case)
                points
                        .forEach(point -> {
                            if(!intersections.containsKey(point)){
                                intersections.put(point,new HashMap<>());
                            }
                            // String with basic instruction based on direction change
                            List<RelativePosition> positions = road.getRelativePositions(new RoadSegment(roadGeometry, false),targetGeometry, Math.sqrt(0.5));
                            // Based on relative position, different instructions are generated
                            if(positions.size() > 0){
                                intersections.get(point).put(targetId, "Odbočte " + positions.get(0).getName());
                                intersections.get(point).put(-targetId, "Odbočte " + RelativePosition.negate(positions.get(0)).getName());
                            }else{
                                if(point.equalsExact(roadGeometry.getStartPoint())) {
                                    intersections.get(point).put(-targetId, "Pokračujte rovně");
                                    intersections.get(point).put(targetId, "Otočte se");
                                }else if(point.equalsExact(roadGeometry.getEndPoint())) {
                                    intersections.get(point).put(targetId, "Pokračujte rovně");
                                    intersections.get(point).put(-targetId, "Otočte se");
                                }else{
                                    intersections.get(point).put(targetId, "Pokračujte rovně");
                                    intersections.get(point).put(-targetId, "Pokračujte rovně");
                                }
                            }
                        });
                // Remember name of this street
                streetName.put(targetId, name);

            }
        } catch (SQLException ex) {
            Logger.getLogger(NearbyCollector.class.getName()).log(Level.SEVERE, "Failed processing POIs", ex);
        }
        // Initiate distances to nearest POI and their details for every intersection
        intersections.forEach((str, point) -> pointDist.put(str, Double.POSITIVE_INFINITY));
        intersections.forEach((str, point) -> poi.put(str,null));
        // Try every row (object) in every table and find possible POI
        for (ResultSet rs : resultMap.values()) {
            try {
                while (rs.next()) {
                    Geometry targetGeometry = Utils.PGtoJTS(rs.getObject("way", PGgeometry.class));
                    for(Geometry point : intersections.keySet()){
                        double dist = point.distance(targetGeometry);
                        // Select only closest POI for each point
                        if(dist > pointDist.get(point) || dist > POI_DIST){
                            continue;
                        }
                        String poiMatch = testKeys(rs, poiList);
                        String name = rs.getString("name");
                        if(poiMatch != null && name != null){
                            pointDist.put(point,dist);
                            poi.put(point,Map.of("name", name, "type", poiMatch));
                        }else if(poiMatch != null && dist + POI_DIST < pointDist.get(point)){
                            // Unnamed objects have distance penalty
                            pointDist.put(point,dist + POI_DIST);
                            poi.put(point,Map.of("type", poiMatch));
                        }
                    }
                }
                // Reset ResultSet
                rs.beforeFirst();
            } catch (SQLException ex) {
                Logger.getLogger(NearbyCollector.class.getName()).log(Level.SEVERE, "Failed processing POIs", ex);
            }
        }
        // Instructions for each intersection and each possible way
        // If POI or street name is missing, create partial instruction
        for (Map.Entry<Geometry, Map<Long, String>> entry : intersections.entrySet()) {
            Geometry key = entry.getKey();
            Map<Long, String> longMap = entry.getValue();
            longMap.keySet()
                    .forEach(id -> longMap.put(id, longMap.get(id) +
                            (poi.get(key) == null ? "" : " u " +
                                    Properties.translatePoiType(poi.get(key).get("type")) +
                                    (poi.get(key).containsKey("name") ? " " + poi.get(key).get("name") : "")
                            ) +
                            (streetName.get(id) == null ? "" : " do ulice " + streetName.get(id))));
        }
        // Return POIs
        List<Map<String, Object>> res = new LinkedList<>();
        // Coordinates have limited number of decimal places
        // Each coordinate is multiplied by 10^shift, rounded and divided by 10^shift
        double shift = Math.pow(10,(Integer) Properties.getSettings().get("DEGREES_PRECISION"));
        poi.forEach((geometry, poiMap) -> res.add(Map.of(
                "geometry",List.of(
                        Math.round(Utils.projectBack(geometry).getCoordinate().x * shift) / shift,
                        Math.round(Utils.projectBack(geometry).getCoordinate().y * shift) / shift),
                "poi", poiMap == null ? "" : poiMap.getOrDefault("name",""),
                "next", intersections.get(geometry)
                )));
        return res;
    }

    /**
     * When this instance is not needed,
     * it closes all ResultSets and returns
     * connection to connection pool
     */
    public void close() {
        // Close every database ResultSet
        resultMap.forEach((dbTable, resultSet) -> {
            try {
                resultSet.close();
            } catch (SQLException e) {
                Logger.getLogger(NearbyCollector.class.getName()).log(Level.SEVERE,"Failed closing ResultSet",e);
            }
        });
        // And finally return connection to connection pool
        try {
            conn.close();
        } catch (SQLException e) {
            Logger.getLogger(NearbyCollector.class.getName()).log(Level.SEVERE,"Failed closing DB connection",e);
        }
        Progress.step();
    }

}
