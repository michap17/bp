package cz.cvut.fel.michalik;

import cz.cvut.fel.michalik.feature.Query;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Static class for properties and settings.
 */
public class Properties {

    /**
     * Properties are weighted categories
     * @return List of properties
     */
    public static Map<String, Query> getProperties() {
        Map<String, Query> properties = new HashMap<>();
        properties.put("roads",
                new Query(Map.of(1.0,Map.of("highway",
                        List.of("motorway","trunk","primary","secondary",
                                "motorway_link","trunk_link","primary_link","secondary_link")),
                                0.01,Map.of("highway",List.of("tertiary","tertiary_link"))),
                        50.0));
        properties.put("green",
                new Query(Map.of(1.0,Map.of(
                                        "natural",List.of("wood"),
                                        "landuse",List.of("forest"),
                                        "leisure",List.of("park","nature_reserve")),
                                0.99,Map.of(
                                        "natural",List.of("tree_row","scrub,heath"),
                                        "landuse",List.of("meadow","orchard","vineyard"),
                                        "leisure",List.of("playground"),
                                        "garden:type",List.of("botanical","castle","community","monastery")),
                                0.01,Map.of(
                                        "natural",List.of("grassland"),
                                        "landuse",List.of("farmland","village_green","grass","greenfield"))),
                        50.0));
        properties.put("water",
                new Query(Map.of(1.0,Map.of(
                        "natural",List.of("water","wetland","glacier","spring","hot_spring","geyser"),
                        "landuse",List.of("basin","reservoir","pond"),
                        "waterway",List.of("river","riverbank","tidal_channel","canal","dam","weir","waterfall")),
                        0.99,Map.of("waterway",List.of("stream","ditch"))),
                        50.0));
        return properties;
    }

    /**
     * List of possible obstacles
     * @return List
     */
    public static Map<String,List<String>> getObstacles() {
        return Map.of(
                "barrier",List.of("city_wall","fence","hedge","retaining_wall","wall"),
                "building",List.of("*"));
    }


    /**
     * List of possible points of interest
     * @return List
     */
    public static Map<String, List<String>> getPOIs() {
        return Map.of(
                "shop",List.of("*"),
                "public_transport",List.of("*"),
                "amenity",List.of("bank","fuel","bus_station","embassy","hospital","clinic","school",
                        "restaurant","pub","cafe","bar","fast_food","food_court","theatre","kindergarten",
                        "pharmacy","library","college","university","fountain","fire_station","police",
                        "shelter", "telephone","playground","grave_yard"),
                "historic",List.of("*")
        );
    }

    /**
     * Application settings and configuration
     * @return Map with settings
     */
    public static Map<String, Object> getSettings(){
        return Map.of(
                "DB_URL", "jdbc:postgresql://localhost/prague",
                "DB_NAME", "petr",
                "DB_PASS", "post",
                "DB_CONNS", 16,
                "DISTANCE", 50.0,
                "OFFSET", 2.0,
                "SEG_LEN", 15.0,
                "SRID", 3035,
                // Decimal places: https://en.wikipedia.org/wiki/Decimal_degrees#Precision
                "DEGREES_PRECISION", 6,
                "FILENAME","export.geojson");
    }

    /**
     * Translates key-value pair to Czech language
     * for better instructions
     * @param type POI key-value pair
     * @return Translated String in second (Genitive) case
     */
    public static String translatePoiType(String type){
        String key = type.split("=")[0];
        String value = type.split("=")[1];
        switch (key) {
            case "shop":
                return "obchodu";
            case "public_transport":
                return "zastávky";
            case "amenity":
                Map<String, String> amenityMap = Map.ofEntries(
                        Map.entry("fuel", "čerpací stanice"), Map.entry("bank", "banky"),
                        Map.entry("bus_station", "zastávky autobusu"), Map.entry("embassy", "ambasády"),
                        Map.entry("hospital", "nemocnice"), Map.entry("clinic", "kliniky"),
                        Map.entry("school", "školy"), Map.entry("restaurant", "restaurace"),
                        Map.entry("pub", "hospody"), Map.entry("cafe", "kavárny"),
                        Map.entry("bar", "baru"), Map.entry("fast_food", "rychlého občerstvení"),
                        Map.entry("theatre", "divadla"), Map.entry("kindergarten", "mateřské školy"),
                        Map.entry("pharmacy", "lékárny"), Map.entry("library", "knihovny"),
                        Map.entry("college", "vysoké školy"), Map.entry("university", "vysoké školy"),
                        Map.entry("fountain", "fontány"), Map.entry("fire_station", "hasičské stanice"),
                        Map.entry("police", "policejní stanice"), Map.entry("shelter", "přístřešku"),
                        Map.entry("telephone", "telefonní budky"), Map.entry("playground", "hřiště"),
                        Map.entry("grave_yard", "hřbitova")
                );
                return amenityMap.getOrDefault(value, "budovy");
            case "historic":
                Map<String, String> historicMap = Map.ofEntries(
                        Map.entry("memorial", "památníku"), Map.entry("wayside_cross", "kříže"),
                        Map.entry("archeological_site", "archeologického naleziště"),
                        Map.entry("ruins", "ruin"), Map.entry("wayside_shrine", "posvátného místa"),
                        Map.entry("monument", "monumentu"), Map.entry("castle", "hradu"),
                        Map.entry("boundary_stone", "hraničního kamene"), Map.entry("tomb", "hrobky"),
                        Map.entry("citywalls", "městské hradby"), Map.entry("mine_shaft", "důlní šachty"),
                        Map.entry("mine", "dolu"), Map.entry("manor", "panského sídla"),
                        Map.entry("milestone", "milníku"), Map.entry("church", "kostela"),
                        Map.entry("fort", "pevnosti"), Map.entry("city_gate", "městské brány"),
                        Map.entry("wreck", "vraku lodi"), Map.entry("farm", "farmy"),
                        Map.entry("aircraft", "vyřazeného letadla"), Map.entry("cannon", "děla"),
                        Map.entry("tower", "věže"), Map.entry("monastery", "kláštera"),
                        Map.entry("locomotive", "vyřazené lokomotivy"), Map.entry("pillory", "pilíře"),
                        Map.entry("tank", "tanku")
                );
                return historicMap.getOrDefault(value, "historického místa");
        }
        return "";
    }
}
