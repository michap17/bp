package cz.cvut.fel.michalik;

import com.google.gson.Gson;
import com.google.gson.stream.JsonWriter;
import cz.cvut.fel.michalik.feature.Query;
import cz.cvut.fel.michalik.geojson.Feature;
import org.geotools.referencing.CRS;
import org.opengis.referencing.FactoryException;

import java.io.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

/**
 * Enriches transport network graph from OpenStreetMap data.
 * Each way contains information about nearby greenery, water and
 * busy roads.
 */
public class Main {

    // Distance in meters
    private static final double DISTANCE = (double) Properties.getSettings().get("DISTANCE");
    // Output filename
    private static final String FILENAME = (String) Properties.getSettings().get("FILENAME");
    // Stream with way ids
    private static Stream<Long> waysStream = null;
    // Stream writer for output file
    private static JsonWriter writer = null;

    public static void main(String[] args) {
        // Shutdown hook saves files with all currently processed roads
        Runtime.getRuntime().addShutdownHook(new Thread(() -> Main.terminate()));
        Logger.getLogger(Main.class.getName()).setLevel(Level.ALL);
        try {
            Connection conn = Connector.connect();
            List<Long> ways = collectAllWays(conn);
            System.out.printf("%d ways found\n", ways.size());
            conn.close();
            // Initialize CRS dataFileCache
            CRS.decode("EPSG:4326");
            // Start progress bar
            Progress.init(ways.size());
            List<DbTable> tables = new LinkedList<>(Arrays.asList(DbTable.READ_POINT, DbTable.READ_LINE, DbTable.READ_POLY));            
            Map<String, Query> properties = Properties.getProperties();
            // Open new File stream
            writer = new JsonWriter(new OutputStreamWriter(new FileOutputStream(FILENAME)));
            // Write header of GeoJSON file
            writer.beginObject();
            writer.name("type").value("FeatureCollection");
            writer.name("features");
            writer.beginArray();
            // Start pool of threads
            waysStream = ways.parallelStream();
            // Process all ways
            waysStream
                    .map(way -> new NearbyCollector(way, DISTANCE, DbTable.READ_WAYS).collectAll(tables))
                    .map(nc -> new Feature(nc, properties))
                    .forEach(Main::writeResult);
            // Exit (run shutdown hook)
            System.exit(0);
        } catch (SQLException | IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } catch (FactoryException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "Failed getting EPSG:4326", ex);
        }
    }
    
    /**
     * Finds all ways in database
     * @param conn DB connection
     * @return List of all way IDs
     * @throws SQLException 
     */
    private static List<Long> collectAllWays(Connection conn) throws SQLException {
        List<Long> resultList = new LinkedList<>();

        Statement stat = conn.createStatement();
        ResultSet rs = stat.executeQuery("SELECT id FROM planet_osm_ways_noded");
        while (rs.next()) {
            long wayID = rs.getLong("id");
            resultList.add(wayID);
        }        

        return resultList;
    }

    /**
     * Thread safely write processed way to output stream
     * @param feature Processed road as Feature
     */
    private static void writeResult(Feature feature){
        try {
            synchronized (writer) {
                if(writer != null) writer.jsonValue(new Gson().toJson(feature));
            }
        } catch (IOException ex){
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, "Could not write to file stream", ex);
        }
    }

    /**
     * Method used in shutdown hook.
     * Closes all connections and streams.
     */
    private static void terminate(){
        if(waysStream != null) {
            waysStream.close();
        }
        Progress.close();
        if(writer != null) {
            synchronized (writer) {
                try {
                    writer.endArray();
                    writer.endObject();
                    writer.close();
                    writer = null;
                    System.out.printf("GeoJSON file saved to %s\n", new File(FILENAME).getCanonicalPath());
                } catch (IOException e) {
                    System.out.printf("Could not save result to %s\n", FILENAME);
                }
            }
        }
    }

}
