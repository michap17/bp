package cz.cvut.fel.michalik;

import com.google.gson.Gson;
import cz.cvut.fel.michalik.geojson.Feature;
import org.postgis.PGgeometry;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

public class RoadSidesTest {

    // OSM ID
    private static final long ROAD_ID = 550307899;
    // Distance in meters
    private static final int DISTANCE = 50;
    private static final double OFFSET = 2;

    public static void main(String[] args) throws FileNotFoundException {
        try(Connection conn = Connector.connect()) {
            // Get road geometry
            PGgeometry road_geom = DistTest2.collectRoad(conn, ROAD_ID);
            // Transform them into features
            HashMap<Double, PGgeometry> sides = new HashMap<>();
            sides.put(1.0, Utils.getSideLine(conn, road_geom, OFFSET));
            sides.put(-1.0, Utils.getSideLine(conn, road_geom, -OFFSET));
            List<Feature> features = sides.keySet().stream()
                    .map(key -> new Feature(Utils.geoToString(conn, sides.get(key)), Map.of("sign",key)))
                    .collect(Collectors.toList());
            // Save it to GeoJSON file
            PrintWriter out = new PrintWriter("export_test_sides.geojson");
            System.out.println("Saved file export_test_sides.geojson");
            // Create GeoJSON string
            String geoJSON = "{\"type\": \"FeatureCollection\", \"features\": " +
                    new Gson().toJson(features) +
                    "}";
            out.print(geoJSON);
            out.close();
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
