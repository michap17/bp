package cz.cvut.fel.michalik;

import cz.cvut.fel.michalik.feature.Query;

import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Test case for single road
 */
public class TestCase {
    private final long roadID;
    private Map<String, Object> result;
    private Map<String,Double> expectedResult;
    private Map<String,Double> resultDiff;
    private String comment;
    private double error;

    public TestCase(long roadID, Map<String, Double> expectedResult) {
        this.roadID = roadID;
        this.expectedResult = expectedResult;
    }

    public TestCase(long roadID, Map<String, Double> expectedResult, String comment) {
        this(roadID,expectedResult);
        this.comment = comment;
    }

    /**
     * Runs test
     * @param properties Properties to be evaluated
     * @param dist Size of search perimeter
     */
    public void test(Map<String, Query> properties, double dist){
        try(NearbyCollector nc = new NearbyCollector(roadID,dist, DbTable.READ_LINE)) {
            this.result = nc.collectAll(null).mapToValues(properties);
        }
        this.error = 0;
        this.resultDiff = new HashMap<>();
        for(Map.Entry<String,Double> entry : expectedResult.entrySet()){
            String key = entry.getKey();
            //assertEquals(expectedResult.get(key), result.get(key),0.1,roadID + ": " + key + " is different");
            if(!result.containsKey(key)){
                System.out.printf("Error, key %s missing\n",key);
                continue;
            }
            double diff = expectedResult.get(key) - (Double) result.get(key);
            error += Math.pow(diff,2);
            if(diff != 0) {
                this.resultDiff.put(key, diff);
            }
        }
    }

    public double getError() {
        return error;
    }

    public long getRoadID() {
        return roadID;
    }

    public Map<String, Double> getResultDiff() {
        return resultDiff;
    }

    /**
     * Returns if this test case is used in given scenario
     * @param key Scenario
     * @return Boolean
     */
    public boolean isScenario(String key){
        double max = expectedResult.entrySet()
                .stream()
                .filter(entry -> entry.getKey().contains(key))
                .map(Map.Entry::getValue)
                .max(Double::compareTo)
                .orElse(0.0);
        return max > 0.0;
    }

    /**
     * Keeps values used only for given scenario
     * @param key Scenario
     * @return Modified TestCase instance
     */
    public TestCase filterExpectedResults(String key){
        this.expectedResult = expectedResult
                .entrySet()
                .stream()
                .filter(map -> map.getKey().contains(key))
                .collect(Collectors.toMap(map -> map.getKey(),map -> map.getValue()));
        return this;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("roadID=").append(roadID);
        sb.append("\nresult  =").append(result);
        sb.append("\nexpected=").append(expectedResult);
        sb.append("\n(exp - res)=").append(resultDiff);
        sb.append("\ncomment='").append(comment).append('\'');
        sb.append("\nerror=").append(error);
        sb.append("\nurl=https://openstreetmap.org/way/").append(roadID).append("\n");
        return sb.toString();
    }

    public String toShortString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("roadID=").append(roadID);
        sb.append("\n(exp - res) =").append(resultDiff);
        sb.append("\ncomment='").append(comment).append('\'');
        return sb.toString();
    }
}
