package cz.cvut.fel.michalik;


import com.google.gson.Gson;
import cz.cvut.fel.michalik.geojson.Feature;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.postgis.PGgeometry;


/**
 * For given road_id finds all nearby objects and saves in them in GeoJSON
 * @author petr
 */
public class DistTest2 {    
    // OSM ID
    private static final long ROAD_ID = 8588965;

    public static void main(String[] args) throws FileNotFoundException {
        try(Connection conn = Connector.connect()) {
            // Get road geometry
            PGgeometry road = collectRoad(conn, ROAD_ID);
            // Distance in meters
            int[] dists = new int[]{25,50,100,250};
            for (int distance : dists){                
                // Find nearby geometries
                List<PGgeometry> nearby = collectInRange(conn, road, distance);
                // Transofrm them into features
                List<Feature> features = nearby.parallelStream()
                        .map(el -> new Feature(Utils.geoToString(conn, el), geoDist(conn, road, el)))
                        .collect(Collectors.toList());
                // Save it to GeoJSON file
                PrintWriter out = new PrintWriter("export_test_" + distance + ".geojson");
                System.out.println("Saved file export_test_" + distance + ".geojson");
                // Create GeoJSON string
                String geoJSON = "{\"type\": \"FeatureCollection\", \"features\": " +
                        new Gson().toJson(features) +
                        "}";
                out.print(geoJSON);
                out.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
    
    /**
     * For given OSM ID of road finds its geometry represenation
     * @param conn DB connection
     * @param id OSM ID
     * @return Geometry
     * @throws SQLException 
     */
    static PGgeometry collectRoad(Connection conn, long id) throws SQLException {

        Statement stat = conn.createStatement();
        ResultSet rs = stat.executeQuery("SELECT way FROM planet_osm_line WHERE osm_id=" + id);
                
        rs.next();
        return rs.getObject("way", PGgeometry.class);           
    }
    
    /**
     * For given road geometry finds nearby objects
     * @param conn DB connection
     * @param road Road geometry
     * @return List of all neaby geomtries
     * @throws SQLException 
     */
    private static List<PGgeometry> collectInRange(Connection conn, PGgeometry road, int distance) throws SQLException{
        List<PGgeometry> resultList = new LinkedList<>();
        List<DbTable> tables = new LinkedList<>(Arrays.asList(DbTable.READ_POINT, DbTable.READ_LINE, DbTable.READ_POLY));
        for (DbTable table : tables){            
            PreparedStatement stat = conn.prepareStatement("SELECT way FROM "+ table +" WHERE ST_DWithin(?, way, ?)");
            stat.setObject(1,road);
            stat.setInt(2, distance);
            ResultSet rs = stat.executeQuery();
            while (rs.next()) {
                PGgeometry obj = rs.getObject("way", PGgeometry.class);                                
                resultList.add(obj);
            }        
        }
        return resultList;
    }
    
    /**
     * Returns distance between 2 geometries
     * @param conn DB connection
     * @param obj1 First geometry
     * @param obj2 Second geometry
     * @return Map with key dist and value as distance between 2 given geometries
     */
    private static Map<String, Object> geoDist(Connection conn, PGgeometry obj1, PGgeometry obj2){
        Map<String, Object> r = new HashMap<>();
        try {
            PreparedStatement ps = conn.prepareStatement("SELECT ST_Distance(?,?) AS d");
            ps.setObject(1, obj1);
            ps.setObject(2, obj2);            
            ResultSet res = ps.executeQuery();
            res.next();            
            r.put("dist", res.getDouble("d"));            
        } catch (SQLException ex) {
            Logger.getLogger(NearbyCollector.class.getName()).log(Level.SEVERE, null, ex);            
        }
        return r;
    }
}
