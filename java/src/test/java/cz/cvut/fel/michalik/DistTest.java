package cz.cvut.fel.michalik;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * ST_DWithin function returns objects in given range, this function gives
 * statistics about count of objects for given distance.
 * ST_Expand expands bounding box around geometry, {@code &&} operator checks if
 * nodes are overlapping
 *
 */
public class DistTest {
    /**
     * Try list of distances to find optimal distance     
     * @throws SQLException 
     */
    public static void main(String[] args) throws SQLException {
        Connection conn = Connector.connect();
        int limit = 10;
        // Can be point, line or poly
        String target = "planet_osm_polygon";
        // Fast method does not check distance
        boolean fastMethod = false;
        List<Integer> dists = Arrays.asList(5, 50, 100, 250, 500, 750, 1000);
        for (Integer dist : dists) {
            tryDist(conn, dist, target, limit, false);
        }
        conn.close();
    }

    /**
     * For given distance writes out statistics (min, mean, max) and execution
     * time.Query only some nodes (given by limit).
     * @param conn Database connection
     * @param distance Distance in meters
     * @param target Target table
     * @param limit Number of tested nodes
     * @throws SQLException 
     */
    public static void tryDist(Connection conn, int distance, String target, int limit, boolean fast) throws SQLException {
        long startTime = System.nanoTime();
        // Test all given nodes, 
        // write number of neighbor nodes
        PreparedStatement statRoads = conn.prepareStatement(""
                + "SELECT roads.osm_id AS road_id, near.osm_id AS near_id "
                + "FROM planet_osm_roads AS roads "
                + "JOIN " + target + " AS near ON "
                + (!fast ? " ST_DWithin(roads.way, near.way, ?) " : "(near.way && ST_Expand(roads.way, ?)) AND (roads.way && ST_Expand(near.way, ?)) ")
                + "WHERE roads.osm_id IN ("
                + "SELECT roads.osm_id AS r_id "
                + "FROM planet_osm_roads AS roads "
                + "LIMIT ?"
                + ")"
        );        
        statRoads.setInt(1, distance);
        if (!fast){
            statRoads.setInt(2, limit);
        }else{
            statRoads.setInt(2, distance);
            statRoads.setInt(3, limit);
        }
        statRoads.setInt(2, limit);
        ResultSet rs = statRoads.executeQuery();
        long endTime = System.nanoTime();
        List<Integer> counts = new ArrayList<>();
        long prevId = 0;
        int sum = 0;
        while (rs.next()) {
            long roadId = rs.getLong("road_id");
            if (prevId == roadId) {
                sum++;
            } else {
                if (prevId != 0) {
                    counts.add(sum);
                }
                prevId = roadId;
                sum = 1;
            }
        }
        counts.add(sum);
        Collections.sort(counts);
        System.out.printf("Distance = %d\nMin = %d \nMean = %d \nMax = %d \nTime = %4.2fs\n", distance,
                counts.get(0), counts.get(counts.size() / 2), counts.get(counts.size() - 1),
                (endTime - startTime) / 10e8);
        System.out.println("=================================");

    }
    
}
