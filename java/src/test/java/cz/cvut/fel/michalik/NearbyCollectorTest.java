package cz.cvut.fel.michalik;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.sql.Connection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 * Test selected ways with scenarios
 */
class NearbyCollectorTest {


    public static void main(String[] args) throws FileNotFoundException {
        double DISTANCE = (double) Properties.getSettings().get("DISTANCE");
        String FOLDER = "tests/";
        Connection conn = Connector.connect();
        Map<String, List<TestCase>> testScenarios = RoadCollection.getScenarios();
        // Create folder for test results
        new File(FOLDER).mkdirs();
        for(Map.Entry<String,List<TestCase>> entry : testScenarios.entrySet()){
            String scenario = entry.getKey();
            //if(scenario != "all") continue;
            List<TestCase> testCases = entry.getValue();
            System.out.println();
            System.out.printf("SCENARIO: %s\n",scenario);
            System.out.printf("Found %d cases\n",testCases.size());
            for(TestCase testCase : testCases){
                //if(testCase.getRoadID() != 4709428) continue;
                testCase.test(Properties.getProperties(),DISTANCE);
            }
            testCases.sort(Comparator.comparingDouble(TestCase::getError));
            //printNCases(testCases,0,testCases.size());
            double error = testCases.stream().map(tc -> tc.getError()).mapToDouble(Double::doubleValue).sum();
            System.out.printf("Total error = %f, average = %f\n",error, error / testCases.size());
            printCasesTex(new PrintStream(FOLDER + scenario + ".txt"),testCases, scenario);
        }
    }

    /**
     * Prints subset of test results
     * @param cases All cases
     * @param start Start index
     * @param end End index
     */
    private static void printNCases(List<TestCase> cases, int start, int end){
        for(int i = start; i < end; ++i){
            System.out.println(cases.get(i).toString());
        }
    }

    /**
     * Prints test results as TeX text.
     * @param out Output stream
     * @param cases All cases
     * @param scenario Test scenario
     */
    private static void printCasesTex(PrintStream out, List<TestCase> cases, String scenario){
        // Based on scenario print only some columns
        boolean printGreen = scenario.equals("green") || scenario.startsWith("all");
        boolean printWater = scenario.equals("water") || scenario.startsWith("all");
        boolean printRoads = scenario.equals("roads") || scenario.startsWith("all");
        // If all columns are used, print table more compact
        if(scenario.startsWith("all")){
            out.print("\\def\\tabiteml{\\enspace} \\def\\tabitemr{\\enspace}");
        }else{
            out.print("\\def\\tabiteml{\\indent} \\def\\tabitemr{\\indent}");
        }
        // Table header has 2 rows
        StringBuilder tableHeaderRow1 = new StringBuilder();
        StringBuilder tableHeaderRow2 = new StringBuilder();
        tableHeaderRow1.append("\\ctable{")
                .append(2 + 2 * ((printGreen ? 1 : 0) + (printWater ? 1 : 0)) + 3 * (printRoads ? 1 : 0))
                .append("r}{\n")
                .append("{} & ");
        tableHeaderRow2.append("ID & ");
        if(printGreen){
            tableHeaderRow1.append("\\mspan2[c]{zeleň} & ");
            tableHeaderRow2.append("podél & skrz & ");
        }
        if(printWater){
            tableHeaderRow1.append("\\mspan2[c]{voda} & ");
            tableHeaderRow2.append("podél & mezi & ");
        }
        if(printRoads){
            tableHeaderRow1.append("\\mspan3[c]{silnice} & ");
            tableHeaderRow2.append("podél & mezi & skrz & ");
        }
        tableHeaderRow1.append("{} \\cr\n");
        tableHeaderRow2.append("chyba \\crl\\tskip4pt\n");
        String tableHeader = tableHeaderRow1.toString() + tableHeaderRow2.toString();
        // Print table header
        out.print(tableHeader);
        // Output 10 worst cases for single categories
        int end = scenario.equals("all") ? cases.size() : Integer.min(cases.size(),10);
        // Descending order for single categories
        if(!scenario.equals("all")) Collections.reverse(cases);
        // Decimal places for all numbers
        String precision = "%.4f";
        for (int i = 0; i < end; ++i) {
            TestCase testCase = cases.get(i);
            Map<String, Double> diff = testCase.getResultDiff();
            out.printf("\\ulink[https://openstreetmap.org/way/%d]{%d} & ", testCase.getRoadID(), testCase.getRoadID());
            if(printGreen){
                out.printf(precision + " & " + precision + " & ",
                        diff.getOrDefault("green_by", 0.0),
                        diff.getOrDefault("green_through", 0.0));
            }
            if(printWater){
                out.printf(precision + " & " + precision + " & ",
                        diff.getOrDefault("water_by", 0.0),
                        diff.getOrDefault("water_between", 0.0));
            }
            if(printRoads){
                out.printf(precision + " & " + precision + " & " + precision + " & ",
                        diff.getOrDefault("roads_by", 0.0),
                        diff.getOrDefault("roads_between", 0.0),
                        diff.getOrDefault("roads_through", 0.0));
            }
            out.printf(precision + " \\cr\n",testCase.getError());
            // Table break
            if ((i + 10) % 45 == 0) {
                out.print("}\n\\maybebreak 10cm\n" + tableHeader);
            }
        }
        out.println("}\n");
    }
}