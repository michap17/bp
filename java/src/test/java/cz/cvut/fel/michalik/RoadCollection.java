package cz.cvut.fel.michalik;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Selected ways for testing.
 * Values of categories are manually set.
 */
public class RoadCollection {

    /**
     * Get all test cases
     * @return List of all selected test cases
     */
    public static List<TestCase> get(){
        List<TestCase> collection = new LinkedList<>();
        collection.add(new TestCase(8588965L, Map.of(
                "green_by",0.01,"green_through",0.01,
                "roads_by",0.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.0,"water_between",0.0),"Technická"));
        collection.add(new TestCase(34260112L,Map.of(
                "green_by",0.6,"green_through",0.005,
                "roads_by",0.0,"roads_between",0.01, "roads_through", 0.0,
                "water_by",0.9,"water_between",0.0), "V Šáreckém údolí - vede podél potoka a lesa"));
        collection.add(new TestCase(550307899L,Map.of(
                "green_by",0.0,"green_through",1.0,
                "roads_by",0.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.0,"water_between",0.0), "Ovál na Letné"));
        collection.add(new TestCase(261944484L,Map.of(
                "green_by",0.0,"green_through",0.6,
                "roads_by",0.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",1.0,"water_between",0.0), "Cesta podél plotu a potoka - Nad Dubovým mlýnem"));
        collection.add(new TestCase(147231964L,Map.of(
                "green_by",0.2,"green_through",0.0,
                "roads_by",0.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",1.0,"water_between",0.0), "Břeh Vltavy před Stromovkou"));
        collection.add(new TestCase(42944842L,Map.of(
                "green_by",0.0,"green_through",1.0,
                "roads_by",0.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.0,"water_between",0.0), "Cesta skrz Divokou Šárku"));
        collection.add(new TestCase(485938101L,Map.of(
                "green_by",0.5,"green_through",0.5,
                "roads_by",0.1,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.0,"water_between",0.0), "Nebušická"));
        collection.add(new TestCase(132401546L,Map.of(
                "green_by",0.1,"green_through",0.2,
                "roads_by",0.1,"roads_between",0.1, "roads_through", 0.0,
                "water_by",0.0,"water_between",0.0), "Podbaba, Ve Struhách - spousta zahrad a městské zeleně"));
        collection.add(new TestCase(141493852L,Map.of(
                "green_by",1.0,"green_through",0.0,
                "roads_by",1.0,"roads_between",0.2, "roads_through", 0.0,
                "water_by",1.0,"water_between",0.0), "Nábřeží mezi Čechovým a Štefánikovým mostem"));
        collection.add(new TestCase(115744295L,Map.of(
                "green_by",0.2,"green_through",0.0,
                "roads_by",1.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.4,"water_between",0.0), "Podbabská"));
        collection.add(new TestCase(132921507L,Map.of(
                "green_by",0.01,"green_through",0.0,
                "roads_by",1.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.0,"water_between",0.0), "Evropská"));
        collection.add(new TestCase(26392190L,Map.of(
                "green_by",0.0,"green_through",1.0,
                "roads_by",0.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.0,"water_between",0.0), "Lesní cesta u Horoměřické"));
        collection.add(new TestCase(26378080L,Map.of(
                "green_by",0.0,"green_through",1.0,
                "roads_by",0.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.4,"water_between",0.0), "Lesní cesta"));
        collection.add(new TestCase(25400091L,Map.of(
                "green_by",0.8,"green_through",0.0,
                "roads_by",0.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",1.0,"water_between",0.0), "Za Elektrárnou"));
        collection.add(new TestCase(44288821L,Map.of(
                "green_by",0.0,"green_through",1.0,
                "roads_by",0.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.0,"water_between",0.0), "Nové Vokovice - Cesta v Divoké Šárce"));
        collection.add(new TestCase(25400107L,Map.of(
                "green_by",0.0,"green_through",1.0,
                "roads_by",0.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.0,"water_between",0.0), "Stromovka"));
        collection.add(new TestCase(25401468L,Map.of(
                "green_by",0.0,"green_through",1.0,
                "roads_by",0.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.8,"water_between",0.0), "Okraj Letné"));
        collection.add(new TestCase(95350183L,Map.of(
                "green_by",0.2,"green_through",0.4,
                "roads_by",0.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.2,"water_between",0.0), "Císařský ostrov - plot"));
        collection.add(new TestCase(8100529L,Map.of(
                "green_by",0.1,"green_through",0.005,
                "roads_by",0.001,"roads_between",0.001, "roads_through", 0.0,
                "water_by",0.0,"water_between",0.0), "Nebušice"));
        collection.add(new TestCase(180965351L,Map.of(
                "green_by",0.5,"green_through",0.2,
                "roads_by",0.1,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.0,"water_between",0.0), "Cukrovarnická"));
        collection.add(new TestCase(360690674L,Map.of(
                "green_by",0.16,"green_through",0.005,
                "roads_by",0.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.0,"water_between",0.0), "Trojská"));
        collection.add(new TestCase(4818889L,Map.of(
                "green_by",0.0,"green_through",1.0,
                "roads_by",0.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.1,"water_between",0.0), "Trojská zámecká zahrada"));
        collection.add(new TestCase(25400106L,Map.of(
                "green_by",0.0,"green_through",1.0,
                "roads_by",0.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.0,"water_between",0.0), "Okraj Stromovky"));
        collection.add(new TestCase(485938113L,Map.of(
                "green_by",0.01,"green_through",0.001,
                "roads_by",0.0,"roads_between",0.01, "roads_through", 0.0,
                "water_by",0.0,"water_between",0.0), "Nebušická"));
        collection.add(new TestCase(562284025L,Map.of(
                "green_by",0.1,"green_through",0.0,
                "roads_by",0.2,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.0,"water_between",0.0), "Chodník mezi Stromovkou a Veletržní"));
        collection.add(new TestCase(32398652L,Map.of(
                "green_by",0.0,"green_through",1.0,
                "roads_by",0.1,"roads_between",0.0, "roads_through", 0.1,
                "water_by",0.0,"water_between",0.0), "Divoká Šárka, okraj Prahy"));
        collection.add(new TestCase(259219167L,Map.of(
                "green_by",0.25,"green_through",0.0,
                "roads_by",0.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.0,"water_between",0.0), "Chodník v Bubenči, podél 'parku' Generála Lázaro Cárdenase"));
        collection.add(new TestCase(182100634L,Map.of(
                "green_by",0.0,"green_through",1.0,
                "roads_by",1.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.0,"water_between",0.0), "Milady Horákové - Letná"));
        collection.add(new TestCase(26378083L,Map.of(
                "green_by",0.1,"green_through",0.5,
                "roads_by",0.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.6,"water_between",0.1), "U Vizerky - Cesta skrz Divokou Šárku"));
        collection.add(new TestCase(4709319L,Map.of(
                "green_by",0.15,"green_through",0.2,
                "roads_by",0.0,"roads_between",0.0, "roads_through", 0.01,
                "water_by",0.0,"water_between",0.0), "Na Šťáhlavce - Juliska"));
        collection.add(new TestCase(677306325L,Map.of(
                "green_by",0.0,"green_through",1.0,
                "roads_by",0.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.6,"water_between",0.4), "Stromovka - Rudolfův rybník"));
        collection.add(new TestCase(548739733L,Map.of(
                "green_by",0.0,"green_through",1.0,
                "roads_by",0.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.3,"water_between",0.7), "Stromovka - Srpeček"));
        collection.add(new TestCase(25400117L,Map.of(
                "green_by",0.0,"green_through",1.0,
                "roads_by",0.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.99,"water_between",0.0), "Stromovka - Malá říčka"));
        collection.add(new TestCase(51277601L,Map.of(
                "green_by",0.2,"green_through",0.0,
                "roads_by",0.1,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.0,"water_between",0.0), "José Martího"));
        collection.add(new TestCase(738125138L,Map.of(
                "green_by",0.0,"green_through",0.008,
                "roads_by",1.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.0,"water_between",0.0), "Pod Kaštany"));
        collection.add(new TestCase(25397203L,Map.of(
                "green_by",0.2,"green_through",0.7,
                "roads_by",0.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.0,"water_between",0.0), "Kaliňák - Divoká Šárka"));
        collection.add(new TestCase(8588962L,Map.of(
                "green_by",0.0,"green_through",0.0,
                "roads_by",0.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.0,"water_between",0.0), "Kulaťák - pěší podchod"));
        collection.add(new TestCase(489293163L,Map.of(
                "green_by",0.5,"green_through",0.005,
                "roads_by",0.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.0,"water_between",0.0), "Tobrucká u Horoměřické"));
        collection.add(new TestCase(34637560L,Map.of(
                "green_by",0.0,"green_through",0.99,
                "roads_by",0.0,"roads_between",1.0, "roads_through", 0.0,
                "water_by",0.0,"water_between",0.0), "Přechod Jugoslávských partyzánů - Rooseveltova"));
        collection.add(new TestCase(546733870L,Map.of(
                "green_by",0.3,"green_through",0.0,
                "roads_by",0.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.0,"water_between",0.0), "Chodník nad Letnou"));
        collection.add(new TestCase(23315134L,Map.of(
                "green_by",0.0,"green_through",1.0,
                "roads_by",0.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.0,"water_between",0.0), "Planetárium Stromovka"));
        collection.add(new TestCase(31957682L,Map.of(
                "green_by",0.75,"green_through",0.0,
                "roads_by",0.2,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.0,"water_between",0.0), "Chodník kolem Letné"));
        collection.add(new TestCase(8660178L,Map.of(
                "green_by",0.2,"green_through",0.001,
                "roads_by",0.1,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.0,"water_between",0.0), "Generála Píky - Dejvice"));
        collection.add(new TestCase(360330376L,Map.of(
                "green_by",0.01,"green_through",0.01,
                "roads_by",0.05,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.0,"water_between",0.0), "Vostrovská - Hanspaulka"));
        collection.add(new TestCase(286839321L,Map.of(
                "green_by",0.15,"green_through",0.005,
                "roads_by",0.05,"roads_between",0.05, "roads_through", 0.0,
                "water_by",0.0,"water_between",0.0), "Chodník kolem Technické"));
        collection.add(new TestCase(561617083L,Map.of(
                "green_by",0.0,"green_through",0.0,
                "roads_by",0.45,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.0,"water_between",0.0), "Chodník u Milady Horákové"));
        collection.add(new TestCase(260181683L,Map.of(
                "green_by",0.01,"green_through",0.0,
                "roads_by",1.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.0,"water_between",0.0), "Chodník podél Evropské"));
        collection.add(new TestCase(4709416L,Map.of(
                "green_by",0.01,"green_through",0.01,
                "roads_by",0.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.0,"water_between",0.0), "Na Babě"));
        collection.add(new TestCase(684198969L,Map.of(
                "green_by",0.01,"green_through",0.005,
                "roads_by",1.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.0,"water_between",0.0), "Cyklostezka + pěší u Evropské"));
        collection.add(new TestCase(27126958L,Map.of(
                "green_by",0.5,"green_through",0.001,
                "roads_by",0.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.3,"water_between",0.0), "Průmyslový palác - Stromovka"));
        collection.add(new TestCase(109076010L,Map.of(
                "green_by",0.01,"green_through",0.0,
                "roads_by",1.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.0,"water_between",0.0), "Kulaťák podél"));
        collection.add(new TestCase(482706951L,Map.of(
                "green_by",0.2,"green_through",0.0,
                "roads_by",0.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.0,"water_between",0.0), "Hanspaulka - podél hřiště s plotem"));
        collection.add(new TestCase(26427953L,Map.of(
                "green_by",0.0,"green_through",0.0,
                "roads_by",0.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.0,"water_between",0.0), "Cesta poblíž Letné - údajně city_wall (jen zábradlí)"));
        collection.add(new TestCase(26427962L,Map.of(
                "green_by",0.0,"green_through",1.0,
                "roads_by",0.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.5,"water_between",0.0), "Chotkovy sady - zeď"));
        collection.add(new TestCase(544563898L,Map.of(
                "green_by",0.1,"green_through",0.0,
                "roads_by",1.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.0,"water_between",0.0), "Královská zahrada - zeď u silnice"));
        collection.add(new TestCase(27123916L,Map.of(
                "green_by",0.0,"green_through",1.0,
                "roads_by",0.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.0,"water_between",0.0), "Královská zahrada - zeď v zahradě (chybí garden:type)"));
        collection.add(new TestCase(4709428L,Map.of(
                "green_by",0.0,"green_through",0.0,
                "roads_by",0.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.0,"water_between",0.0), "Šlejnická - plot"));
        collection.add(new TestCase(482520327L,Map.of(
                "green_by",0.0,"green_through",1.0,
                "roads_by",0.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",1.0,"water_between",0.0), "Stromovka - podél vodního kanálu"));
        collection.add(new TestCase(677306334L,Map.of(
                "green_by",0.0,"green_through",1.0,
                "roads_by",0.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.5,"water_between",0.0), "Stromovka - kousek vody"));
        collection.add(new TestCase(165956907L,Map.of(
                "green_by",0.01,"green_through",0.0,
                "roads_by",0.0,"roads_between",0.0, "roads_through", 0.0,
                "water_by",0.9,"water_between",0.0), "V Šáreckém údolí"));


        /* PATTERN
        collection.add(new TestCase(L,Map.of(
                "green_by",0.0,"green_through",0.0,
                "roads_by",0.0,"roads_between",0.0,
                "water_by",0.0,"water_between",0.0), ""));
        */
        return collection;
    }

    /**
     * Gets all scenarios with test cases
     * @return List of all testing scenarios
     */
    public static Map<String, List<TestCase>> getScenarios(){
        List<String> scenariosKeys = List.of("green","water","roads");
        Map<String, List<TestCase>> scenarios = new HashMap<>();
        List<Long> obstacleScenario = List.of(95350183L,115744295L,261944484L,4818889L,489293163L,
                482706951L,26427953L,26427962L,544563898L,27123916L,4709428L,165956907L);
        for(String scenarioKey : scenariosKeys) {
            scenarios.put(scenarioKey, get()
                    .stream()
                    .map(testCase -> testCase.filterExpectedResults(scenarioKey))
                    .filter(testCase -> testCase.isScenario(scenarioKey))
                    .collect(Collectors.toList()));
        }
        scenarios.put("all_obstacles",get()
                .stream()
                .filter(testCase -> obstacleScenario.contains(testCase.getRoadID()))
                .collect(Collectors.toList())
        );
        scenarios.put("all",get());
        return scenarios;

    }
}
