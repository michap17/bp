#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

# SETUP
sudo -u postgres createuser -s petr
sudo -u postgres psql -c "ALTER USER petr WITH ENCRYPTED PASSWORD 'post';"

